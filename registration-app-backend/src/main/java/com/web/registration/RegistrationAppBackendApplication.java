package com.web.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrationAppBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationAppBackendApplication.class, args);
	}

}
