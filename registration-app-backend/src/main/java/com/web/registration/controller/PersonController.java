package com.web.registration.controller;

import com.web.registration.model.entity.Person;
import com.web.registration.model.request.PersonCreate;
import com.web.registration.service.PersonService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/online-registration")
public class PersonController {

    private final HttpServletRequest request;

    @Autowired
    private PersonService personService;

    @Autowired
    public PersonController( HttpServletRequest request) {
        this.request = request;
    }

    @PostMapping("/person")
    public ResponseEntity<Person> createPerson(@ApiParam(value = "The Person to be created" ,required=true )  @Valid @RequestBody PersonCreate person) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return personService.createPerson(person);
        }
        return new ResponseEntity<Person>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/persons")
    public ResponseEntity<List<Person>> listPerson(@ApiParam(value = "Returns a list of Persons") @Valid @RequestParam(value = "fields", required = false) String fields, @ApiParam(value = "Requested index for start of resources to be provided in response") @Valid @RequestParam(value = "offset", required = false) Integer offset, @ApiParam(value = "Requested number of resources to be provided in response") @Valid @RequestParam(value = "limit", required = false) Integer limit) {
        return personService.listPerson();
    }
}
