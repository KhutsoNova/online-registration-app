package com.web.registration.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @JsonProperty("name")
    @Column(name="name")
    @NotNull
    public String name;

    @JsonProperty("surname")
    @Column(name="surname")
    @NotNull
    public String surname;

    @JsonProperty("idNumber")
    @Column(name="idNumber")
    @NotNull
    public String idNumber;

    @JsonProperty("telephoneNumber")
    @Column(name="telephoneNumber")
    @NotNull
    public String telephoneNumber;

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getSurname() { return surname; }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIdNumber() { return idNumber; }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getTelephoneNumber() { return telephoneNumber; }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(this.id, person.id) &&
                Objects.equals(this.name, person.name) &&
                Objects.equals(this.surname, person.surname) &&
                Objects.equals(this.idNumber, person.idNumber) &&
                Objects.equals(this.telephoneNumber, person.telephoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, idNumber, telephoneNumber);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Person {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    surname: ").append(toIndentedString(surname)).append("\n");
        sb.append("    idNumber: ").append(toIndentedString(idNumber)).append("\n");
        sb.append("    telephoneNumber: ").append(toIndentedString(telephoneNumber)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}
