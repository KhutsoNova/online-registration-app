package com.web.registration.service.Interface;

import com.web.registration.model.entity.Person;
import com.web.registration.model.request.PersonCreate;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PersonApi {

    ResponseEntity<Person> createPerson(PersonCreate newPerson);

    ResponseEntity<List<Person>> listPerson();
}
