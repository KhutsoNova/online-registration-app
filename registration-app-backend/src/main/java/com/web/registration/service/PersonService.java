package com.web.registration.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.registration.model.entity.Person;
import com.web.registration.model.request.PersonCreate;
import com.web.registration.repository.PersonRepository;
import com.web.registration.service.Interface.PersonApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("PersonService")
public class PersonService implements PersonApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PersonRepository personRepository;

    public ResponseEntity<Person> createPerson(PersonCreate newPerson) {
        try {

            if (newPerson.getName() == null || newPerson.getName().isEmpty() || newPerson.getSurname() == null || newPerson.getSurname().isEmpty()
            || newPerson.getIdNumber() == null || newPerson.getIdNumber().isEmpty() || newPerson.getTelephoneNumber() == null || newPerson.getTelephoneNumber().isEmpty()) {
                LOGGER.error("All fields must be populated when creating a record of a new person");
                return new ResponseEntity<Person>(HttpStatus.BAD_REQUEST);
            }

            if (!newPerson.getIdNumber().matches("[a-zA-Z ]*\\d+.*") || !newPerson.getTelephoneNumber().matches("[a-zA-Z ]*\\d+.*")) {
                LOGGER.error("The IdNumber and telephoneNumber fields must be numeric values");
                return new ResponseEntity<Person>(HttpStatus.BAD_REQUEST);
            }

            Person person = new Person();
            person.setName(newPerson.getName());
            person.setSurname(newPerson.getSurname());
            person.setIdNumber(newPerson.getIdNumber());
            person.setTelephoneNumber(newPerson.getTelephoneNumber());
            personRepository.save(person);

            LOGGER.info("The following Person was created with ID Number: " + person.getIdNumber());
            return new ResponseEntity<Person>(objectMapper.readValue("{  \"id\" : \""+person.getId()+"\", \"name\" : \""+person.getName()+"\",  \"surname\" : \""+person.getSurname()+"\",  \"idNumber\" : \""+person.getIdNumber()+"\", \"telephoneNumber\" : \""+person.getTelephoneNumber()+"\"}", Person.class), HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("The following exception occurred: ", e);
            return new ResponseEntity<Person>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    public ResponseEntity<List<Person>> listPerson() {
        try {
            List<Person> persons = personRepository.findAll();

            if(persons.size() == 0){
                LOGGER.info("No Persons found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(persons,HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("The following exception occurred: ", e);
            return new ResponseEntity<List<Person>>(HttpStatus.EXPECTATION_FAILED);
        }
    }

}
