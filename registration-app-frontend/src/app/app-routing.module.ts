import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonListComponent } from './component/person-list/person-list/person-list.component';
import { PersonFormComponent } from './component/person-form/person-form/person-form.component';


const routes: Routes = [
  { path: 'report', component: PersonListComponent },
  { path: 'register-person', component: PersonFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
