export class Person {
    id: string;
    name: string;
    surname: string;
    idNumber: string;
    telephoneNumber: string;
}
