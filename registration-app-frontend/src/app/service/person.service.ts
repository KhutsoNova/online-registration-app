import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Person } from '../model/person';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private backendUrl: string;

  constructor(private http: HttpClient) {
    this.backendUrl = 'http://localhost:8080';
  }

  public save(person: Person) {
    return this.http.post<Person>(this.backendUrl+'/online-registration/person', person);
  }

  public findAll(): Observable<Person[]> {
    return this.http.get<Person[]>(this.backendUrl+'/online-registration/persons');
  }
 

}
